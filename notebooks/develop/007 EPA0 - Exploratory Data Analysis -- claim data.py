
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Submission-dates-around-financial-year-end" data-toc-modified-id="Submission-dates-around-financial-year-end-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Submission dates around financial year end</a></span></li><li><span><a href="#Distribution-of-claims-by-value" data-toc-modified-id="Distribution-of-claims-by-value-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Distribution of claims by value</a></span><ul class="toc-item"><li><span><a href="#Staffing-costs" data-toc-modified-id="Staffing-costs-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Staffing costs</a></span></li></ul></li><li><span><a href="#Biggest-and-smallest-claimers" data-toc-modified-id="Biggest-and-smallest-claimers-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Biggest and smallest claimers</a></span><ul class="toc-item"><li><span><a href="#Biggest-and-smallest-claimers---average-by-financial-year" data-toc-modified-id="Biggest-and-smallest-claimers---average-by-financial-year-3.1"><span class="toc-item-num">3.1&nbsp;&nbsp;</span>Biggest and smallest claimers - average by financial year</a></span><ul class="toc-item"><li><span><a href="#Bar-plot" data-toc-modified-id="Bar-plot-3.1.1"><span class="toc-item-num">3.1.1&nbsp;&nbsp;</span>Bar plot</a></span></li></ul></li></ul></li></ul></div>

# # Exploring the features in the dataset

# In this notebook we'll have a dig around the features and perhaps see what feature engineering can be done.
# 
# From this point on we'll work with the Pickle file created in [006 DC2 - Data Cleaning - joining tables](006%20DC2%20-%20Data%20Cleaning%20-%20joining%20tables.ipynb)

# In[9]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import sqlite3
import pickle

# set retina display -- makes plots look much better.
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('retina')


# In[8]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
PICKLE_FILE = f'{DATA_DIR}expenses.pickle'
MP_PICKLE_FILE = f'{DATA_DIR}mps.pickle'
PARTY_COLOURS_FILE = f'{DATA_DIR}party_colours.pickle'
# print floats with a thousands separator and 2 decimal places.
pd.options.display.float_format = '{:,.2f}'.format


# In[3]:


all_data = pd.read_pickle(PICKLE_FILE)


# In[4]:


all_data.info()


# ## Submission dates around financial year end

# Note that these are the dates the expenses were submitted, not the dates they were incurred,. Note that there is a large concentration around the end of March/beginning of April, which is the financial year end.

# In[5]:


all_data['date'].value_counts()


# The below plot shows that expenses seem to all be submitted around a quarter into the year, as this must be the finanical year cut-off. 

# In[6]:


plt.figure(figsize=(15,8))

sns.lineplot(data=all_data.sample(100000), x='date', y='amount_claimed', ci=None);


# ## Distribution of claims by value

# Lets plot the distribution of expense claim values.

# In[7]:


plt.figure(figsize=(15,8))

sns.distplot(all_data['amount_claimed']);


# Wow. There are some extremely large outliers... Let's check it out.

# ### Staffing costs

# In[118]:


all_data[['mps_name', 'date', 'party', 'expense_type', 'short_description', 'amount_claimed']]    .sort_values(by=['amount_claimed'], ascending=False).head(30)


# Ok. So some MPs claim a whole year's worth of payroll in one hit... Lets look into the Payroll abit more.

# In[35]:


payroll_claims = all_data[all_data['expense_type'] == 'Payroll']


# In[36]:


plt.figure(figsize=(15,8))

sns.distplot(payroll_claims['amount_claimed']);


# In[76]:


payroll_claims['amount_claimed'].mean()


# So the average payroll claim is ~£105k, but we're not sure if every claim is annualised, so we need to not presume this to be an annual number. Something to look into later....

# ## Biggest and smallest claimers

# So who are the biggest and smallest claimers over the entire period?

# In[5]:


all_data.groupby(['mps_name'])    .sum()[['amount_claimed', 'amount_paid', 'amount_not_paid', 'amount_repaid']]    .sort_values('amount_claimed', ascending=False)


# Lets break this down by financial year, as some MPs have been around much longer than others.

# In[5]:


# build the pivot table
# See https://stackoverflow.com/questions/41383302/pivot-table-subtotals-in-pandas
# for how to do subtotals...

table = pd.pivot_table(all_data,
                       index=['mps_name', 'year'],
                       values=['amount_claimed', 'amount_paid',
                               'amount_not_paid', 'amount_repaid'],
                       aggfunc='sum'
                       )


# In[6]:


table


# So what's the average annual claim by MP?

# In[7]:


averages = table.groupby(level='mps_name').mean()
averages.head()


# Lets add the party data first...

# In[8]:


mps_data = pd.read_pickle(MP_PICKLE_FILE)


# In[9]:


averages_with_party = averages.merge(mps_data[['name', 'party']],
                                     left_index=True,
                                     right_on=['name'],
                                     how='left',
                                     validate='1:1'
                                     )
averages_with_party


# ### Biggest and smallest claimers - average by financial year

# In[10]:


sorted_averages = averages_with_party.sort_values(by='amount_claimed', ascending=False)
sorted_averages


# #### Bar plot

# In[79]:


def change_width(ax, new_value):
    for patch in ax.patches:
        current_height = patch.get_height()
        diff = abs(new_value - current_height)

        # we recenter the bar
        patch.set_y(patch.get_y() + diff * 0.5)

        # we change the bar width
        patch.set_height(new_value)


# In[10]:


party_colours = {
    'Alliance': 'grey',
    'Conservative': 'dodgerblue',
    'DUP': 'skyblue',
    'Deputy Speaker': 'grey',
    'Green': 'lawngreen',
    'Independent': 'grey',
    'Independent Labour': 'lightsalmon',
    'Labour': 'tomato',
    'Labour/Co-operative': 'salmon',
    'Liberal Democrat': 'gold',
    'Plaid Cymru': 'yellowgreen',
    'Respect': 'fuchia',
    'Scottish National Party': 'yellow',
    'Sinn Féin': 'grey',
    'Social Democratic and Labour Party': 'coral',
    'Speaker': 'grey',
    'UKIP': 'purple',
    'UUP': 'grey'
}


# In[11]:


with open(PARTY_COLOURS_FILE, 'wb') as part_col_file:
    pickle.dump(party_colours,part_col_file)


# In[95]:


plt.figure(figsize=(18,40))
sns.set()
plt.title('Top 100 Average Annual MP Expense Claims')
ax = sns.barplot(data=sorted_averages.head(100), 
                 x='amount_claimed', 
                 y='name', 
                 hue='party', 
                 palette=party_colours, 
                 dodge=False)
# note uneven bars is a known bug when you turn on the hue parameter but for some reason dodge=False fixes it!
plt.legend(loc='lower right')
change_width(ax, .6)

ax.set(xlabel='Expenses in GBP', ylabel='Member of Parliament')
plt.show();


# These top 3 all have constituencies very far from westminster -- perhaps it's travel costs, perhaps not... 
