
# coding: utf-8

# # Feature Engineering - start up / wind up costs, hire vs purchase, staff vs MP, etc

# In this workbook we will try to engineer some addition features. The areas which I'd be looking at here are:
# 
# - **start-up, wind-up and BAU costs** -- essentially, there seems to be expenses incurred in setting up a constituency office for a new MP, operating it as BAU, and then winding up when the MP loses their seat (at least, this is my understanding). So we'll attempt to categorise the expenses into these 3 types.
# - **hire vs purchase** -- some MPs like to hire stuff, others like to purchase it - there might be value in flagging this.
# - **MPs own costs versus costs incurred by MPs' staff or dependents** - lets see if we can break this out.

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import sqlite3

# set retina display -- makes plots look much better.
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('retina')


# In[2]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
PICKLE_FILE = f'{DATA_DIR}expenses.pickle'
MP_PICKLE_FILE = f'{DATA_DIR}mps.pickle'
# print floats with a thousands separator and 2 decimal places.
pd.options.display.float_format = '{:,.2f}'.format


# In[3]:


all_data = pd.read_pickle(PICKLE_FILE)


# In[7]:


all_data['category'].value_counts()


# Lets take a look at MPs own expenses... Well, first the category called 'MP Travel' is different to 'Staff Travel' so this is a good place to start. Lets see if we can engineer a field off this.

# In[24]:


all_data[all_data['category'] == 'MP Travel'].head(5)


# Lets also look at "Staff Travel"...

# In[23]:


all_data[all_data['category'] == 'Staff Travel'].head(5)


# what about dependents?

# In[22]:


all_data[all_data['category'] == 'Dependant Travel'].head(5)


# Ok, so these look reasonable as is.
# 
# Lets move on to hiring vs purchase.

# In[28]:


all_data[all_data['short_description'].str.contains('purchase', na=False)]

