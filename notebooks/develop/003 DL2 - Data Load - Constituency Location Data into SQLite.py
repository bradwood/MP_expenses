
# coding: utf-8

# # Get Constituency Location Data

# This notebook will obtain Latitude/Longitude (latlong) data from TWFY and compute the distance of the constituency from Parliament in Westminster. Clearly this data is important insofar as travel and/or second home costs are concerned for MPs travelling to and from Parliament, or living far from Westminster.

# In[1]:


import numpy as np
import pandas as pd
import sqlite3
import requests
from time import sleep
import json


# In[2]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
DB_FILE = f'{DATA_DIR}expenses.db'
TWFY_KEY = '/Users/brad/Code/DS-ML-AI/MP_expenses/TWFY.key'
conn = sqlite3.connect(DB_FILE)


# In[3]:


with open(TWFY_KEY, 'r') as apif:
    api_key = apif.readlines()[0].rstrip()


# Let's define a function to fetch constituency geo data from TWFY. The `/getGeometry` endppint at TWFY provides a variety of geographical data about the constituency supplied. The response it provides shown in this example:
# 
# ```json
#      {
#        "max_e" : 535714.299409,
#        "srid_en" : 27700,
#        "area" : 9062132.09829,
#        "max_n" : 188327.40118,
#        "min_lat" : 51.5423943861,
#        "max_lat" : 51.5777842936,
#        "centre_n" : 186576.286843,
#        "max_lon" : -0.0439261708385,
#        "centre_lon" : -0.0723445316926,
#        "min_n" : 184364.195635,
#        "parts" : 1,
#        "centre_e" : 533716.511394,
#        "min_e" : 531479.800928,
#        "min_lon" : -0.104510690585,
#        "centre_lat" : 51.5622217259,
#        "name" : "Hackney North and Stoke Newington"
#      }
# 
# 
# ```
# 
# However, this data is not consistently provided, in particular, for Northern Ireland constituencies, the API only provides the centroid coordinates of the constituency, judged by eye. The fields we are interested in are:
# 
# - `area` - the area of the constituency in square metres
# - `centre_lat` - the latitude measure (in decimal degrees) of the centroid (centre) of the constituency
# - `centre_lon` - the longitude measure (in decimal degrees) of the centroid (centre) of the constituency
# 
# Using the centroid co-ordinates, plus the co-ordinates of Westminster itself we will be able to calculate the distance of the centre of the constituency from Westminster itself using the [Haversine Formula](https://en.wikipedia.org/wiki/Haversine_formula)

# Let's define a function to calculate the distance between two points on the earth's surface using the Haversine formula. Code from [here](https://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points).

# In[4]:


from math import radians, cos, sin, asin, sqrt

def haversine(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance in kilometers between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6372.8  # Radius of earth in kilometers. Use 3956 for miles
    return c * r


# And now we can define the function to fetch the data from TWFY and compute the distance from Westminster as we insert it into our local database.

# In[5]:


def get_geog_data(db_conn, key):
    geog_data_list = []
    houses_of_parliament_lat_long = (51.5005076,-0.1263934)
    parl_lat, parl_lon = houses_of_parliament_lat_long
    get_geo_url = 'https://www.theyworkforyou.com/api/getGeometry'

    const_list = pd.read_sql_query("""SELECT distinct constituency 
                                      FROM members
                                      ORDER by constituency;
                                   """, 
                                   db_conn)
    
    drop_table_if_exists = 'DROP TABLE IF EXISTS geo_data;'
    create_table = """
        CREATE TABLE geo_data (
            "constituency" TEXT PRIMARY KEY NOT NULL UNIQUE,
            "centre_lat" REAL,
            "centre_lon" REAL,
            "area" REAL,
            "dist_from_parl" REAL
        );
        """

    cur = db_conn.cursor()
    cur.execute(drop_table_if_exists)
    cur.execute(create_table)
    db_conn.commit()

    for _, con in const_list.iterrows():
        params = {
            'name': con,
            'output': 'js',
            'key': key,
        }
        response = requests.get(get_geo_url,params=params)
        print(f'Got data for {con}...')
        sleep(0.2)
        data = response.json()
        print(data)
        if data.get('error',False):
            print(f"No data for {con}: {data['error']}, skipping..")
            continue

        insert_tuple = (
                data['name'],
                data['centre_lat'],
                data['centre_lon'],
                data.get('area',None), # in case it's black for NI consts
                haversine(data['centre_lat'],data['centre_lon'],parl_lat,parl_lon)
                )

        cur.execute(f'''INSERT INTO geo_data 
                        VALUES (?,?,?,?,?)
                     ''', insert_tuple)
        db_conn.commit()
        print(f'Saved data for {con}...')


# In[6]:


get_geog_data(conn, api_key)


# And finally, indexes...

# In[7]:


def create_geog_info_indexes(conn):
    """Create database indexes for the geo_data table."""
    cur = conn.cursor()
    cur.execute('CREATE INDEX IF NOT EXISTS geo_data_constituency ON geo_data("constituency")')
    cur.execute('CREATE INDEX IF NOT EXISTS geo_data_centre_lat ON geo_data("centre_lat")')
    cur.execute('CREATE INDEX IF NOT EXISTS geo_data_centre_lon ON geo_data("centre_lon")')
    cur.execute('CREATE INDEX IF NOT EXISTS geo_data_area ON geo_data("area")')
    cur.execute('CREATE INDEX IF NOT EXISTS geo_data_dist_from_parl ON geo_data("dist_from_parl")')
    conn.commit()
    print('Done')


# In[8]:


create_geog_info_indexes(conn)

