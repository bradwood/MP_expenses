
# coding: utf-8

# # Getting MP Expenses Data

# This data is sourced from http://www.theipsa.org.uk/. After some digging I found this piece of HTML and JavaScript which controls the download form the site.
# 
# ```html
# <div class="download-data-panel">
#     <span class="data-download-text">
#         You can select a year and type of data to download
#     </span>
#     <span class="data-download-option">
#         <label for="year-select">Year</label>
#         <select id="year-select">
#                 <option value="6">2010 to 2011</option>
#                 <option value="5">2011 to 2012</option>
#                 <option value="4">2012 to 2013</option>
#                 <option value="3">2013 to 2014</option>
#                 <option value="2">2014 to 2015</option>
#                 <option value="1">2015 to 2016</option>
#                 <option value="7">2016 to 2017</option>
#                 <option value="8">2017 to 2018</option>
#         </select>
#     </span>
#     <span class="data-download-option">
#         <label for="data-type-select">Type</label>
#         <select id="data-type-select">
#             <option value="aggregate">Total Spend</option>
#             <option value="other">Other Data</option>
#             <option value="claims">Individual claims</option>
#         </select>
#     </span>
#     <span class="data-download-option">
#         <span id="download-panel-download-databtn" class="button handyman-fancy-button download-databtn"><i class="fa fa-download"></i>Download</span>
#     </span>
# </div>
# <script>
#     (function($) {
#         $('#download-panel-download-databtn').click(function(e) {
#             var yearid = $('#year-select').val();
#             var dataType = $('#data-type-select').val();
#             var url = "";
#             if (dataType === "aggregate") {
#                 url = "/download/DownloadAggregateCsv/";
#             } else if (dataType === "other") {
#                 url = "/download/DownloadOtherInfoCsv/";
#             } else {
#                 url = "/download/DownloadClaimsCsv/";
#             }
# 
#             url += yearid;
# 
#             //this should actually trigger a download, but try to open in new tab anyway to leave page pointing where it is.
#             window.open(url);
#         });
#     })($)
# </script>
# ```

# The below loads a number of python functions to fetch the data off the web and insert it into a local SQL database for subsequent analysis.

# In[6]:


get_ipython().run_line_magic('autoreload', '2')


# In[7]:


from data_loaders.get_expenses import download_expenses_csvs, write_expenses_to_db, create_indexes


# This function downloads each CSV from the URLs identified from the HTML file above.

# In[8]:


download_expenses_csvs()


# This function loads all the `individual_claims` CSV files and inserts them into a local SQLite database file for later querying.

# In[9]:


write_expenses_to_db()


# And then it creates some database indexes to make querying faster.

# In[10]:


create_indexes()

