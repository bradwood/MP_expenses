
# coding: utf-8

# # Data Cleansing - Matching on MP names

# This notebook will investigate missing foreign keys, missing data and other bits and pieces to see if this can be fixed or imputed. It requires that all ordinally prior notebooks to this one (based on the first 3 characters of the file name) are  run in order to create the SQLite database that we work with in this notebook. See the list below:

# In[165]:


get_ipython().run_line_magic('ls', '00[1-3]*.ipynb')


# In[166]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import sqlite3


# In[167]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
DB_FILE = f'{DATA_DIR}expenses.db'
conn = sqlite3.connect(DB_FILE)


# Lets get the data from SQLite. We'll join one table at a time to understand what data might be missing from the join. If we can fix it, we will.

# In[168]:


expenses_table = pd.read_sql_query('SELECT * from expenses', conn)


# In[169]:


expenses_table.info()


# Note that not every claim has a claim number. And that we have **1,445,964** records in the table.

# So now we know the total number of expense claims in the database, lets investigate joining in the party data from the members table. We will want to join on name because we don't have decent referential integrity from the expenses datasource and TWFY.

# Now lets check out the members table.

# In[170]:


members_table = pd.read_sql_query('SELECT * from members', conn)


# In[171]:


members_mp_names = members_table['name'].unique()


# In[172]:


expenses_mp_names = expenses_table['mps_name'].unique()


# In[173]:


print('no of members in expenses table:', len(expenses_mp_names))
print('no of members in members table:', len(members_mp_names))


# Lets see who's missing from the members table

# In[174]:


def look_for_mis_matched_mp_names():
    missing = 0
    for mp in expenses_mp_names:
        if mp not in members_mp_names:
            missing += 1
            print(f'{missing}: {mp} not found in members_mp_names')


# In[175]:


look_for_mis_matched_mp_names()


# There are weirdnesses in the name matching accross the two tables. `Ed` vs `Edward`, `Ken`, vs `Kenneth`, etc.. Let see if we can fix these. Lets try to match just on surname and see what we get.

# In[176]:


def look_for_close_matched_mp_names():
    missing = 0
    for mp in expenses_mp_names:
        if mp not in members_mp_names:
            lastname = mp.split(" ")[len(mp.split(" "))-1]
            close_matches = pd.read_sql_query(f'SELECT name from members where name like "%{lastname}%"', conn)
            if not close_matches.empty:
                missing += 1
                print(f'{missing}: EXP: {mp} not found in members_mp_names')
                print(close_matches)


# In[178]:


look_for_close_matched_mp_names()


# Here's a function below that can fix an eyeballed list of name mismatches. Note, I am re-running this notebook iteratively after every fix to see what the upper cells yeild... Hopefully fewer mismatches on every pass.
# 

# In[179]:


def fix_names_in_data():
    name_map = {
        'Andy Love': 'Andrew Love',
        'Anne Marie Morris': 'Anne Morris', 
        'Brian H Donohoe':'Brian Donohoe',
        'Dame Cheryl Gillan': 'Cheryl Gillan',
        'Chi Onwurah': 'Chinyelu Onwurah',
        'Chris Pincher': 'Christopher Pincher',
        'Chris Leslie': 'Christopher Leslie',
        'Dan Byles': 'Daniel Byles',
        'Diana R. Johnson': 'Diana Johnson',
        'Ed Balls': 'Edward Balls',
        'Ed Miliband': 'Edward Miliband',
        'Ed Vaizey': 'Edward Vaizey',
        'Jeffrey M. Donaldson': 'Jeffrey Donaldson',
        'Jenny Willott': 'Jennifer Willott',
        'Jim Hood': 'Jimmy Hood',
        'Jon Ashworth': 'Jonathan Ashworth',
        'Ken Clarke': 'Kenneth Clarke',
        'Mary Macleod': 'Mary MacLeod',
        'Mike Crockart': 'Michael Crockart',
        'Mike Weir': 'Michael Weir',
        'Nic Dakin': 'Nicholas Dakin',
        'Nick Boles': 'Nicholas Boles',
        'Rob Flello': 'Robert Flello',
        'Steve McCabe': 'Stephen McCabe',
        'Steve Baker': 'Steven Baker',
        'Willie Bain': 'William Bain',
        'Nick Brown': 'Nicholas Brown',
        'Andy Slaughter': 'Andrew Slaughter',
        'Steve Pound': 'Stephen Pound',
        'Vince Cable': 'Vincent Cable',
        'John Martin McDonnell': 'John McDonnell',
        'Liz McInnes': 'Elizabeth Anne McInnes',
        'Rebecca Long Bailey': 'Rebecca Long-Bailey',
        'Tom Tugendhat': 'Thomas Tugendhat',
        'Chris Huhne': 'Christopher Huhne',
        'Chris Elmore': 'Christopher Elmore',
        'Dr Caroline Johnson': 'Caroline Johnson',
        'Susan Elan Jones': 'Susan Jones',
    }
    cur = conn.cursor()
    for wrong_name, right_name in name_map.items():
        sql = f"""UPDATE members
                 SET name = '{right_name}'
                 WHERE name = '{wrong_name}'
              """
        print(sql)
        cur.execute(sql)
        conn.commit()
        
        sql2 = f"""UPDATE expenses
                 SET mps_name = '{right_name}'
                 WHERE mps_name = '{wrong_name}'
              """
        print(sql2)
        cur.execute(sql2)
        conn.commit()


# In[180]:


fix_names_in_data()


# One more thing though, is to deal with MPs that actually share a number with another. There are 2 sets that I found the Mike Woods and the Angela Smiths... Lets make these more recognisable:
# 
# - [Michael Roy Wood](https://en.wikipedia.org/wiki/Mike_Wood_(Labour_politician)) a Labour polititian.
# - [Michael Jon Wood](https://en.wikipedia.org/wiki/Mike_Wood_(Conservative_politician)) a Conservative Politican.
# 
# - [Angela Smith](https://en.wikipedia.org/wiki/Angela_Smith_(Sheffield_MP)) 
# - [Baroness Angela Smith](https://en.wikipedia.org/wiki/Angela_Smith,_Baroness_Smith_of_Basildon)
# 
# These should be easy to sort out.
# 

# In[184]:


def fix_duplicate_MPs():
    cur = conn.cursor()
    sql = f"""UPDATE expenses
         SET mps_name = 'Mike R. Wood'
         WHERE mps_name = 'Mike Wood'
         AND mps_constituency = 'Batley and Spen BC'
      """
    print(sql)
    cur.execute(sql)
    conn.commit()

    sql = f"""UPDATE members
         SET name = 'Mike R. Wood'
         WHERE name = 'Mike Wood'
         AND party = 'Labour'
      """
    print(sql)
    cur.execute(sql)
    conn.commit()

    sql = f"""UPDATE expenses
         SET mps_name = 'Mike J. Wood'
         WHERE mps_name like '%Wood'
         AND mps_constituency = 'Dudley South BC'
      """
    print(sql)
    cur.execute(sql)
    conn.commit()

    sql = f"""UPDATE members
         SET name = 'Mike J. Wood'
         WHERE name = 'Mike Wood'
         AND party = 'Conservative'
      """
    print(sql)
    cur.execute(sql)
    conn.commit()
    
    sql = f"""UPDATE members
         SET name = 'Angela C. Smith'
         WHERE name = 'Angela Smith'
         AND constituency like '%Penistone and Stocksbridge%'
      """
    print(sql)
    cur.execute(sql)
    conn.commit()

    sql = f"""UPDATE expenses
         SET mps_name = 'Angela C. Smith'
         WHERE mps_name = 'Angela Smith'
         AND mps_constituency like '%Penistone and Stocksbridge%'
      """
    print(sql)
    cur.execute(sql)
    conn.commit()


    


# In[183]:


fix_duplicate_MPs()

