
# coding: utf-8

# ## Data Cleansing

# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import sqlite3
get_ipython().run_line_magic('matplotlib', 'inline')


# Todo:
# 
# Exploratory:
#  - wind up
#  - pairplots /correlation
#  - biggest hirers
#  - biggest claims rejected
#  - largest expenses by type  - DONE
#  - hospitality?
#  - staff vs MPs own expenses
#  
# Feature engineering:
#  
#  - party - DONE
#  - latling and distance from westminster. - DONE
#  - hired / bought -- WON'T DO - data unreliable.
#  - staff / mp / dependent -- ALREADY THERE
#  - travel - DONE
#  - payroll / non-payroll  - ALREADY THERE
#  - wind-up / start-up - ALREADY THERE
#  - expense type mapping - DONE
# 
# Data Aggregation/Prep
#  - roll-ups (otherwise **WAY** to slow)
#      - sum of all expenses by type, by MP, by year
#      - numerical data only
#      - data by year
# 
# Analysis:
#  
#  - correlations of claims to:
#      - party
#      - distance from parliament
#      - staff costs
#      - non-staff costs
#      
#  - unsupervised learning
#      - cluster analysis
#      - principal component analysis
#      - anomaly detection
#  
# Visualisations:
#  
#  - chloropleth - expenses by constituency.
