
# coding: utf-8

# # Feature engineering - better expense categorisation

# In[277]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import sqlite3

# set retina display -- makes plots look much better.
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('retina')


# In[278]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
PICKLE_FILE = f'{DATA_DIR}expenses.pickle'
MP_PICKLE_FILE = f'{DATA_DIR}mps.pickle'
# print floats with a thousands separator and 2 decimal places.
pd.options.display.float_format = '{:,.2f}'.format


# In[279]:


all_data = pd.read_pickle(PICKLE_FILE)


# Lets check out the expense categories:

# In[280]:


all_data['category'].value_counts()


# It looks like there are lots of different types of travel. Lets investigate the travel expenses claims further. The first thing we'll do is to add a boolean field to capture any type of travel.

# In[281]:


# create a new field which flags any type of travel claim
def ends_with_travel(s):
    return s.endswith('Travel')


# In[282]:


all_data['is_travel'] = all_data['category'].apply(ends_with_travel)


# In[283]:


all_data.head()


# Lets save with the new field.

# In[284]:


all_data.to_pickle(PICKLE_FILE)


# Now lets check out the `expense_type` column in a bit more detail.

# In[285]:


all_data['expense_type'].value_counts()


# There are loads of different expense types, lets try to clean this up a bit.

# In[286]:


type_map = {
    'taxi': ['taxi', 'cab'],
    'car hire': ['Car Hire'],
    'hotel': ['hotel'],
    'accomodation': ['accomodation rent', 
                     'Accommodation Rent', 
                     'Accommodation', 
                     'Ground Rent', 
                     'Accom Rent', 
                     'Accomm Rent', 
                     'Accom Service Chgs',
                    ],
    'rent (offices)': ['const office hire of premises', 
                       'const office rent', 
                       'Office Rent', 
                       'Budget Repayment (OC)', 
                       'Rent (Wind Up)', 
                       'Rent (Wind. Up)'
                       'services charges',
                       'service charge',
                      ],
    'subsitence': ['food', 
                   'drink'
                  ],
    'phone': ['telephone', 
              'mobile', 
              'Landline', 
              'Const Office Tel.', 
              'Const Off Tel.Usg', 
              'Accom Tel',
              'Blackberry',
             ],
    'internet/website/software': ['internet', 
                                  'Website', 
                                  'Software Purchase', 
                                  'Comp SW Purch StartUp',
                                  'software',
                                 ],
    'utilities': ['electricity', 
                  'gas', 
                  'water',
                 ],
    'mileage': ['own car', 
                'Own Vehicle', 
                'Own Motorcycle/Scooter MP', 
                'Own Bicycle', 
                'Congest. Zone',
                'congestion Zone',
                'Other Fuel',
               ],
    'aggregated travel (ni)': ['Aggregated Travel'],
    'tax': ['council tax', 
            'Business Rates',
           ],
    'public transport': ['public tr rail', 
                         'Public Tr COACH', 
                         'Public Tr FERRY',
                         'Public Tr Underground', 
                         'Public Tr UND',
                         'Public Tr Bus',
                         'Public Tr Season Ticket',
                         'Public Tr',
                        ],
    'flights': ['public tr air'],
    'postage': ['postage',
                'Mailing',
                'post office',
                'post',
                'Royal Mail',
               ],
    'staff/payroll': ['payroll', 
                      'Employment Costs', 
                      'Staff Costs',
                      'Staffing',
                      'Staff One Off Payments (Wind Up)',
                     ],
    'pooled services': ['Pooled Services: Direct', 
                        'Pooled Staffing Services', 
                        'Pooled Services'
                       ],
    'insurance': ['insurance', 
                  'Const Office Buildings Insur', 
                  'Bldng Insur', 
                  'Legal Exp/Emp Practice Insur.', 
                  'Accom Buildgs Insur', 
                  'Office Contents Insur.'
                 ],
    'interest': ['Mortgage Interest'],
    'equipment/office service': ['Shredding', 
                                 'Photocopier', 
                                 'Shredder', 
                                 'Scanner', 
                                 'Fax', 
                                 'Office Equip. StartUp',
                                 'Install/Maint Office Equip', 
                                 'Printer', 
                                 'Equipment Hire', 
                                 'Comp HW Purch StartUp',
                                 'Comp HW Purchase StartUp',
                                 'Computer',
                                 'Other Equip Purchase',
                                 'Photocopy service',
                                 'maintenance',
                                 'Office equipment',
                                ],

    'office furniture': ['Office Furniture Purchase', 
                         'Office Furniture Hire', 
                         'Furniture Purchase', 
                         'Furniture Hire', 
                         'Office Furn Purch StartUp',
                        ],
    'television': ['Television', 
                   'Accom TV Licence (Wind. Up)',
                   'Purchase of TV StartUp',
                  ],
    'stationery': ['Stationery',
                   'Office Consumables StartUp',
                   'letterhead',
                   'letter head',
                  ],
    'professional services': ['Professional Services', 
                              'Parliamentary Accountancy', 
                              'Language Services', 
                              'translation',
                              'Legal',
                              'information commissioner',
                              'ico.gov.uk',
                              'data protection',
                              'Information Commission',
                              'ICO',
                              'Information Commisioner',
                              'Information Comissioner',
                             ],
    'other': [
              'Parliamentary Account Wind. Up',
              'Miscellaneous',
             ],
    'parking': ['parking'],
    'advertising': ['advertising'],
    'hospitality': ['hospitality'],
    'contingency': ['Contingency'],
    'venue hire': ['Venue Hire'],
    'training': ['Training'],
    'security': ['Security',
                 'Accom Routine Secur (Wind. Up)',
                ],
    'recruitment': ['Recruitment'],
    'removal costs': ['Removal Costs', 
                      'Office Removals'],
    'contact cards': ['Contact Cards'],
    'loan': ['Residential Deposit Loan'],
    'media': ['Newspapers/Journals',
              'newspaper', 
              'gallery news',
              'news paper',
              'press',
              'Newsagent',
              'paper',
             ],
    'waste/recycing': ['Waste Disposal',
                       'Recycling',
                       'waste',
                      ],
    'cleaning/hygiene': ['cleaning',
                         'hygeine',
                         'toilet',
                         'hygiene'
                        ],
    'health and welfare': ['Health and Welfare Costs'],
    'maintenance/repairs': ['Const Office repairs', 
                            'Dilapidations', 
                            'Redecorating',
                            'Office Alterations StartUp',
                            'Refurbishment',
                            'refurb',
                            'repairs',
                            'repair',
                            'decal',
                            'signs',
                           ],
    'volunteer agreed arragement costs': ['Volunteer Agreed Arrang. Costs'],
    'reward & recognition': ['Reward and Recognition Payment'],
    'budget repayment': ['Budget Repayment '],
    'outside scheme': ['Expense Outside Scheme'],
    'misc travel': ['Travel Costs','Travel'],
    'office sundries': ['tea',
                        'coffee',
                        'milk',
                        'sugar',
                        'hot chocolate',
                        'wipes',
                        'sundries',
                        'bulbs',
                        'cleaner',
                        'sacks',
                        'sack',
                        'liner',
                        'office supplies',
                        'wash',
                        'batteries',
                        'photocopying',
                        'Photocopies',
                        'photocopy',
                        'printing',
                        'copier',
                        'Toner',
                        'key',
                        'Consumables',
                        'cloth',
                        'duster',
                        'kitchen',
                        'Office Miscellaneous',
                        'business card',
                        'kettle',
                        'Extension lead',
                        'bins',
                        'banner',
                        'soap',
                        'sanitiser',
                        'sanitizer',
                       ]
}


# In[287]:


def map_type(input_field):
    for new_cat, list_of_matches in type_map.items():
        for match in list_of_matches:
            if not input_field:
                return 'unknown'            
            if match.lower() in input_field.lower():
                return new_cat
    return 'unknown'


# In[288]:


expense_mapping_by_type = all_data['expense_type'].apply(map_type)
expense_mapping_by_type.value_counts()


# There still seems to be an aweful lot of "unknown". Lets see if we can fix that by applying the `map_type()` function on the `short_description` field, and the `detail` field for these unknowns.

# In[289]:


expense_mapping_by_short_desc = all_data['short_description'].apply(map_type)


# In[290]:


expense_mapping_by_details = all_data['details'].apply(map_type)


# In[291]:


expense_map_3_ways = pd.concat([expense_mapping_by_type,expense_mapping_by_short_desc,expense_mapping_by_details], axis=1)


# In[292]:


expense_map_3_ways.replace('unknown', np.nan, inplace=True);


# Ok, so now we have the expense mapping mapped 3 different ways. Lets apply a function to these 3 columns which will select the most likely categorisation

# In[293]:


expense_map_3_ways.head()


# In[294]:


def select_likely_expense_category(row):    
    if row[0] is not np.nan:
        return row[0]
    if row[1] is not np.nan:
        return row[1]
    if row[2] is not np.nan:
        return row[2]
    return 'unknown'
    


# In[295]:


expense_mapping = expense_map_3_ways.apply(select_likely_expense_category,axis=1)


# In[301]:


expense_mapping.value_counts()


# In[297]:


all_data['expense_mapping'] = expense_mapping


# In[298]:


all_data[all_data['expense_mapping'] == 'unknown'].sample(frac=1)


# Ok, this looks much better, and will be good enough I think. Lets save the dataset as it is now.

# In[299]:


all_data.to_pickle(PICKLE_FILE)


# In[300]:


all_data.info()

