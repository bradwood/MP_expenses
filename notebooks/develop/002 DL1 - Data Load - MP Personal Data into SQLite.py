
# coding: utf-8

# # Getting MP Party Data 

# In[1]:


import numpy as np
import pandas as pd
import sqlite3
import requests
from time import sleep
import json


# Now we need to get the personal and party affiliation data for each MP. We'll use the API at https://www.theyworkforyou.com/ (TWFY) to do this.
# 
# The API at TWFY has a `/getMPs` endpoint but unfortunately it only provides data for MPs that are currently sitting when passed the MPs name. This is no good for our purposes as there are a fair number of MPs who incurred expenses who are no longer serving. 
# 
# I emailed the support people at TWFY to ask if there was an enpoint that would serve every MP's personal data for as long back as they have data but, alas, not. So, the approach we will adopt is to download the full MP list via the `/getMPs` endpoint for every year that we have expense data. We'll then need to match our expense data against a cleaned up and de-duped list of MPs over the entire period.

# Set some constants for the database and the API key for TWFY.

# In[2]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
DB_FILE = f'{DATA_DIR}expenses.db'
TWFY_KEY = '/Users/brad/Code/DS-ML-AI/MP_expenses/TWFY.key'
conn = sqlite3.connect(DB_FILE)


# Load the TWFY API Key -- note, you will need to get your own key by registering on the site. Then simply place the key in the `TWFY.key` file.

# In[3]:


with open(TWFY_KEY, 'r') as apif:
    api_key = apif.readlines()[0].rstrip()


# Now we define a function to download the MP data.
# 
# This function returns only those MPs that were in parliament at that date submitted. The number varies from around 630 in the 70s to around 650 in 2018 with a peak at the turn of the millenium of 659. 
# 
# Clearly there will be a lot of duplicate data for the career MPs from year to year -- we'll need to de-dupe this data later.
# 
# Interestingly, the data on a particular MP, when it is provided for a given year, is occasionally up to date to the current period. So, for example,  take Diane Abbott.  This is the query posted:
# 
# ` https://www.theyworkforyou.com/api/getMPs?date=2000-01-01&output=js`
# 
# and this is her record:
# 
# ```json
# {
#   "member_id" : "1",
#   "person_id" : "10001",
#   "name" : "Diane Abbott",
#   "party" : "Labour",
#   "constituency" : "Hackney North and Stoke Newington",
#   "office" : [{
#   "dept" : "",
#   "position" : "Shadow Home Secretary",
#   "from_date" : "2016-10-06",
#   "to_date" : "9999-12-31"
# }]
# ```
# 
# Note that it has information about her current office, even though this is for the year 2000.
# 
# However, annoyingly, the `office` data appears unreliable. Lets look at Gordon Brown who was Chancellor of the Exchequer from 1997 to 2007. When we use the same query as above, we just get:
# ```json
# {
#   "member_id" : "68",
#   "person_id" : "10068",
#   "name" : "Gordon Brown",
#   "party" : "Labour",
#   "constituency" : "Dunfermline East"
# }
# ```
# ... no mention of the office held. So, for this reason **we'll ignore the `office` fields for now**, although we may be able to get this via another TWFY endpoint down the line. 
# 
# Note also the `person_id` and `member_id` fields. The `person_id` seems an obvious unique key for a person (whether an MP/former MP or not). The `member_id` changes from one parliament to another.

# In[4]:


def load_MP_personal_data(from_year, to_year, key, db_conn):
    """Download personal MP data."""

    get_MPs = 'https://www.theyworkforyou.com/api/getMPs'
    drop_table_if_exists = 'DROP TABLE IF EXISTS members;'
    create_table = """
        CREATE TABLE members (
            "person_id" INTEGER PRIMARY KEY NOT NULL UNIQUE,
            "member_id" INTEGER NOT NULL,
            "name" TEXT,
            "party" TEXT,
            "constituency" TEXT
        );
    """
    cur = db_conn.cursor()
    cur.execute(drop_table_if_exists)
    cur.execute(create_table)
    db_conn.commit()
    
    for year in range(from_year, to_year):
        params = {
            'date': f'{year}-01-01',
            'key': key,
        }
        inserts = []
        raw_data = requests.get(get_MPs, params=params)
        data = raw_data.json()
        sleep(0.5)  # go easy on the API
        print(f'{len(data)} records to insert for {year}')
        for entry in data:
            member_id = entry['member_id']
            person_id = entry['person_id']
            name = entry['name']
            party = entry['party']
            constituency = entry['constituency']     
            # insert a tuple of each item into the inserts list.
            inserts.append((person_id,member_id,name,party,constituency))
        cur.executemany('INSERT OR REPLACE INTO members VALUES (?,?,?,?,?)', inserts)
        print(f'Data inserted.')
        db_conn.commit()
        


# In[5]:


load_MP_personal_data(2010,2018, api_key, conn)


# Let's see how many members records we have:

# In[10]:


pd.read_sql_query("select count(name) from members",conn)


# ...and how many MP names we have in the expenses table:

# In[11]:


pd.read_sql_query("select count(*) from (select distinct mps_name from expenses)",conn)


# so we are probably okay as we have more MP names that we have MPs that filed expenses...

# As a final task for this table lets add some indexes to the database to speed things up.

# In[12]:


def create_member_indexes(conn):
    """Create database indexes for the member table."""
    cur = conn.cursor()
    cur.execute('CREATE INDEX IF NOT EXISTS members_name ON members("name")')
    cur.execute('CREATE INDEX IF NOT EXISTS members_party ON members("party")')
    cur.execute('CREATE INDEX IF NOT EXISTS members_constituency ON members("constituency")')
    cur.execute('CREATE INDEX IF NOT EXISTS members_person_id ON members("person_id")')
    cur.execute('CREATE INDEX IF NOT EXISTS members_member_id ON members("member_id")')
    conn.commit()
    print('Done')


# In[13]:


create_member_indexes(conn)


# # Getting additional MP data

# There is some additional data that might be of interest that's worth getting, namely:
# 
# - EU referendum stance (`eu_ref_stance`)
# - DOB (`date_of_birth`)
# - facebookpage (`facebook_page`)
# - website (`mp_website`)
# - twitter username (`twitter_username`)
# - wikipedia_url (`wikipedia_url`)
# - bbc_url (`bbc_profile_url`)
# - link to a photo on parliament.uk (`photo_attribution_link`)
# 
# This data is also available from TWFY via the `/getMPInfo` endpoint. Lets write a function that gets this extra data for each MP.

# In[8]:


def load_extra_MP_data(db_conn, key):
    """Get additional MP data from TWFY."""
    mp_list = pd.read_sql_query("""SELECT person_id, name 
                                   FROM members 
                                   ORDER by name;
                            """, db_conn)
    
    get_MPInfo = 'https://www.theyworkforyou.com/api/getMPInfo'
    
    drop_table_if_exists = 'DROP TABLE IF EXISTS member_info;'
    
    api_field_list = """eu_ref_stance,
                    date_of_birth,
                    facebook_page,
                    mp_website,
                    twitter_username,
                    wikipedia_url,
                    bbc_profile_url,
                    photo_attribution_link"""
    
    create_table = """
        CREATE TABLE member_info (
            "person_id" INTEGER PRIMARY KEY NOT NULL UNIQUE,
            "eu_ref_stance" TEXT,
            "date_of_birth" DATE,
            "facebook_page" TEXT,
            "website" TEXT,
            "twitter_username" TEXT,
            "wikipedia_url" TEXT,
            "bbc_profile_url" TEXT,
            "photo_link" TEXT
        );
    """
    cur = db_conn.cursor()
    cur.execute(drop_table_if_exists)
    cur.execute(create_table)
    db_conn.commit()
    
    for _, mp in mp_list.iterrows():
        print(mp)
        params = {
            'id': mp['person_id'],
            'key': key,
            'fields': api_field_list, 
        }
        raw_data = requests.get(get_MPInfo, params=params)
        print(f'Got data for {mp["name"]}...')
        print(raw_data.text)
        if raw_data.text == 'null':
            print(f'No data for {mp["name"]}, skipping..')
            continue
        data = raw_data.json()
        sleep(0.2)
        insert_tuple = (
                        mp['person_id'],
                        data.get('eu_ref_stance',None),
                        data.get('date_of_birth',None),
                        data.get('facebook_page',None),
                        data.get('website',None),
                        data.get('twitter_username',None),
                        data.get('wikipedia_url',None),
                        data.get('bbc_profile_url',None),
                        data.get('photo_link',None)
                        )
        
        cur.execute(f'''INSERT INTO member_info 
                        VALUES (?,?,?,?,?,?,?,?,?)
                     ''', insert_tuple)
        db_conn.commit()
        print(f'Saved data for {mp["name"]}...')


    
    
    
    


# In[9]:


load_extra_MP_data(conn, api_key)


# Lastly, indexes.

# In[14]:


def create_member_info_indexes(conn):
    """Create database indexes for the member_info table."""
    cur = conn.cursor()
    cur.execute('CREATE INDEX IF NOT EXISTS member_info_person_id ON member_info("person_id")')
    conn.commit()
    print('Done')


# In[15]:


create_member_info_indexes(conn)

