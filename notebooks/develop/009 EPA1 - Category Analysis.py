
# coding: utf-8

# # Look at Expense Categories

# This notebook will look at the various expense categories.

# In[13]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import pickle

# set retina display -- makes plots look much better.
from IPython.display import set_matplotlib_formats
set_matplotlib_formats('retina')


# In[14]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
PICKLE_FILE = f'{DATA_DIR}expenses.pickle'
MP_PICKLE_FILE = f'{DATA_DIR}mps.pickle'
PARTY_COLOURS_FILE = f'{DATA_DIR}party_colours.pickle'
# print floats with a thousands separator and 2 decimal places.
pd.options.display.float_format = '{:,.2f}'.format


# Load all the data and the party_colours dictionary for charting purposes.

# In[15]:


all_data = pd.read_pickle(PICKLE_FILE)


# In[19]:


with open(PARTY_COLOURS_FILE, 'rb') as part_col_file:
    party_colours = pickle.load(part_col_file)
party_colours


# To prepare the dataset for correlation analysis and plotting lets do the following:
# 
# - drop uneccessary columns
# 

# In[22]:


all_data.info()


# In[26]:


numerical_data = all_data.drop([
    
                           'claim_no',
                           'category',
                           'expense_type',
                           'short_description',
                           'details', 
                           'journey_type',
                           'from',
                           'to',
                           'travel',
                           'nights',
                           'mileage',
                           'reason_if_not_paid',
                           'amount_claimed',
                           'amount_not_paid',
                           'amount_repaid',
                           'status',
                           'expense_id',
                           'person_id',
                          ], 
                          axis=1 
                         )
numerical_data.head()


# ## TODO -- move this to a new workbook -- Create an aggregated dataset

# This dataset is too large for some charts, so lets aggregate.

# In[35]:


numerical_data.groupby(by=['year','party','mps_name','mps_constituency','date','expense_mapping'], squeeze=True).sum()


# Lets now apply one-hot encoding to the party field....

# In[24]:


# numerical_data = pd.concat([numerical_data, pd.get_dummies(numerical_data['party'], prefix='party')], axis=1)
# numerical_data.drop(['party'], axis=1, inplace=True)
# numerical_data.head()


# And similarly, to the expense_mapping field....

# In[8]:


# numerical_data = pd.concat([numerical_data, pd.get_dummies(numerical_data['expense_mapping'], prefix='exp_map')], axis=1)
# numerical_data.drop(['expense_mapping'], axis=1, inplace=True)
# numerical_data.head()


# In[25]:


numerical_data.info()


# In[12]:


sns.set()
g = sns.catplot(y='expense_mapping',
                x='amount_paid', 
                hue='party',
                ci=None,
                kind='swarm',
                data=numerical_data.sample(frac=0.0035),
                legend_out=False,
                dodge=False,
               )

g.fig.set_size_inches(30,15)

