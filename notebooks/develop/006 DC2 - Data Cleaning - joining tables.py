
# coding: utf-8

# # Joining tables

# In this notebook we'll try join all our data together to build a reliable dataset that can be used for analysis. Make sure you've run the below notebooks first.

# In[1]:


get_ipython().run_line_magic('ls', '00[0-5]*.ipynb')


# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import sqlite3


# In[3]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
DB_FILE = f'{DATA_DIR}expenses.db'
PICKLE_FILE = f'{DATA_DIR}expenses.pickle'
MP_PICKLE_FILE = f'{DATA_DIR}mps.pickle'
conn = sqlite3.connect(DB_FILE)


# Lets get the expenses data first, then join in the other data.

# In[4]:


expenses_table = pd.read_sql_query('SELECT * from expenses', conn)


# In[5]:


expenses_table.info()


# In[6]:


expense_and_party =  pd.read_sql_query('''SELECT expenses.*, 
                                          members.party,
                                          members.person_id
                                          FROM expenses 
                                          LEFT JOIN members 
                                          ON expenses.mps_name = members.name
                                       ''', conn)





# In[7]:


expense_and_party.info()


# This looks okay, although we note that party data is not present for every MP. Now lets join in the rest of the data..

# In[8]:


expense_party_dob =  pd.read_sql_query('''SELECT expenses.*, 
                                          members.party,
                                          members.person_id,
                                          member_info.date_of_birth
                                          FROM expenses 
                                          LEFT JOIN members 
                                          ON expenses.mps_name = members.name
                                          LEFT JOIN member_info
                                          ON members.person_id = member_info.person_id
                                       ''', conn)


# In[9]:


expense_party_dob.info()


# Not as many DOBs as we would have hoped... But we know the data itself is relatively patchy by looking athe below queries.

# In[10]:


pd.read_sql_query('select count(person_id) from member_info where date_of_birth not null',conn)


# In[11]:


pd.read_sql_query('select count(person_id) from member_info',conn)


# Lets join in the next table now, `geo_data`.

# In[12]:


all_data =  pd.read_sql_query('''SELECT expenses.*, 
                                 members.party,
                                 members.person_id,
                                 member_info.date_of_birth,
                                 geo_data.area,
                                 geo_data.dist_from_parl
                                 FROM expenses 
                                 LEFT JOIN members 
                                 ON expenses.mps_name = members.name
                                 LEFT JOIN member_info
                                 ON members.person_id = member_info.person_id
                                 LEFT JOIN geo_data
                                 ON expenses.mps_constituency = geo_data.constituency
                               ''', conn)


# In[13]:


all_data.info()


# Lets fix the dates

# In[14]:


all_data['date'] = pd.to_datetime(all_data['date'])


# In[15]:


all_data['date_of_birth'] = pd.to_datetime(all_data['date_of_birth'])


# In[16]:


all_data.info()


# Ok, the constituency geo data looks good. Next step is to save this dataframe down as a baseline. 

# In[17]:


all_data.to_pickle(PICKLE_FILE)


# Now lets make a clean MPs data table for subsequent use:

# In[18]:


mps_data = pd.read_sql_query('''select name, party, constituency, date_of_birth 
                             from members 
                             join member_info 
                             on members.person_id = member_info.person_id''', conn)


# In[19]:


mps_data.head()


# In[20]:


mps_data.to_pickle(MP_PICKLE_FILE)

