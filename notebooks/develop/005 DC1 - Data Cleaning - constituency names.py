
# coding: utf-8

# # Cleaning Constituency Names

# Lets check out the names of constituencies to make sure these are matchable.

# This notebook will fix up the constituency names so we can join on these. It requires that the previous notebooks have been executed in order.

# In[37]:


get_ipython().run_line_magic('ls', '00[1-4]*.ipynb')


# In[38]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import seaborn as sns
import sqlite3


# In[39]:


DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
DB_FILE = f'{DATA_DIR}expenses.db'
conn = sqlite3.connect(DB_FILE)


# Lets get the data from SQLite. We'll join one table at a time to understand what data might be missing from the join. If we can fix it, we will.

# In[40]:


expenses_table = pd.read_sql_query('SELECT * from expenses', conn)


# In[41]:


members_table = pd.read_sql_query('SELECT * from members', conn)


# In[42]:


geodata_table = pd.read_sql_query('SELECT * from geo_data', conn)


# In[43]:


expenses_constituencies = expenses_table['mps_constituency'].unique()


# In[44]:


members_constituencies = members_table['constituency'].unique()


# In[45]:


geodata_constituencies = geodata_table['constituency'].unique()


# In[51]:


print('no of constituencies in expenses table:', len(expenses_constituencies))
print('no of constituencies in members table:', len(members_constituencies))
print('no of constituencies in geodata table:', len(geodata_constituencies))


# In[52]:


expenses_constituencies


# In[53]:


members_constituencies


# In[54]:


geodata_constituencies


# It just looks like we need to strip off the 'BC' and 'CC' from the constituency field in the expenses table.

# In[34]:


def remove_BC_CC_from_constit():
    cur = conn.cursor()
    sql = f"""UPDATE expenses
              SET mps_constituency = REPLACE(mps_constituency, ' BC', '')
          """
    print(sql)
    cur.execute(sql)
    conn.commit()
    
    sql = f"""UPDATE expenses
              SET mps_constituency = REPLACE(mps_constituency, ' CC', '')
          """
    print(sql)
    cur.execute(sql)
    conn.commit()


# In[35]:


remove_BC_CC_from_constit()


# In[36]:


conn.close()

