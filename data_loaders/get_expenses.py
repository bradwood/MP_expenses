"""Misc Tools for downloading and prepping MP Expense Data."""

import requests
from time import sleep
import numpy as np
import pandas as pd
import os
import sqlite3

DATA_DIR = '/Users/brad/Code/DS-ML-AI/MP_expenses/data/'
DB_FILE = f'{DATA_DIR}expenses.db'

def download_expenses_csvs(force=False):
    """Download CSV data from http://www.theipsa.org.uk """

    baseURL = 'http://www.theipsa.org.uk'

    agg_link = {
        'name': 'aggregate',
        'url': '/download/DownloadAggregateCsv/'
    }
    other_link = {
        'name': 'other',
        'url': '/download/DownloadOtherInfoCsv/'
    }

    ind_claims_link = {
        'name': 'individual_claims',
        'url': '/download/DownloadClaimsCsv/'
    }

    links = [agg_link, other_link, ind_claims_link]

    years = {
        '2010-2011': '6',
        '2011-2012': '5',
        '2012-2013': '4',
        '2013-2014': '3',
        '2014-2015': '2',
        '2015-2016': '1',
        '2016-2017': '7',
        '2017-2018': '8',
    }

    for y_range, num in years.items():
        for link_dict in links:
            url = baseURL + link_dict["url"] + str(num)
            filename = f'{DATA_DIR}{link_dict["name"]}_{y_range}.csv'
            if not os.path.isfile(filename) and not force:
                print(f'Getting {y_range} data from {url} and saving to {filename}')
                response = requests.get(url, stream=True)
                with open(filename, 'wb') as f:
                    f.write(response.content)
                sleep(5)  # go easy on the web server
            else:
                print(f'{filename} exists. Not downloading...')




def write_expenses_to_db():
    """Write CSV data to SQLite """

    dtypes = {
        "Year":  np.object,
        "Claim No.": np.object,
        "MP's Name": np.object,
        "MP's Constituency": np.object,
        "Category": np.object,
        "Expense Type": np.object,
        "Short Description": np.object,
        "Details": np.object,
        "Journey Type": np.object,
        "From": np.object,
        "To": np.object,
        "Travel": np.object,
        "Nights": np.float64,
        "Mileage": np.float64,
        "Amount Claimed": np.float64,
        "Amount Paid": np.float64,
        "Amount Not Paid": np.float64,
        "Amount Repaid": np.float64,
        "Status": np.object,
        "Reason If Not Paid": np.object,
    }
    converters = {
        'Date': pd.to_datetime,
    }

    conn = sqlite3.connect(DB_FILE)
    index_prefix = 0
    for file in os.listdir(DATA_DIR):
        if file.startswith('individual_claims') and file.endswith('csv') and not os.path.isfile(f'{DATA_DIR}{file}.loaded'):
            i = 1
            index_prefix += 1
            for df in pd.read_csv(f'{DATA_DIR}{file}', dtype=dtypes, converters=converters, chunksize=1024*8, iterator=True):
                print(f'{file} chunk {i} read...')
                columns = df.columns
                columns = [i.replace(' ', '_') for i in columns]
                columns = [i.replace("'", '') for i in columns]
                columns = [i.replace(".", '') for i in columns]
                columns = [i.lower() for i in columns]
                df.columns = columns
                # manufacture a primary key as we know that some expenses don't have a claim_no.
                df['expense_id'] = df['year'] + '_' + df.index.astype(str)
                print(df.head())
                #write the pandas dataframe to a sqlite table
                df.to_sql('expenses', con=conn,
                          if_exists='append', index=False)
                print(f'{file} chunk {i} written...')
                i += 1
            # touch file after it's loaded.
            open(f'{DATA_DIR}{file}.loaded', 'a').close()
        elif file.startswith('individual_claims') and file.endswith('csv'):
            print(f'{file} already loaded...')

    conn.close()


def delete_empty_rows():
    """Delete empty rows"""
    conn = sqlite3.connect(DB_FILE)
    cur = conn.cursor()
    cur.execute('delete from expenses where year is null')
    conn.commit()
    conn.close()
    print("Done")


def create_indexes():
    """Index the database after loading expenses."""
    conn = sqlite3.connect(DB_FILE)
    cur = conn.cursor()
    cur.execute('CREATE UNIQUE INDEX IF NOT EXISTS expenses_expense_id ON expenses(expense_id)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_category ON expenses(category)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_expense_type ON expenses(expense_type)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_date ON expenses(date)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_status ON expenses(status)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_year ON expenses(year)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_mps_name ON expenses(mps_name)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_mps_constituency ON expenses(mps_constituency)')
    cur.execute('CREATE INDEX IF NOT EXISTS expenses_amount_claimed ON expenses(amount_claimed)')
    conn.commit()
    conn.close()
    print("Done")


if __name__ == '__main__':
    download_expenses_csvs()
    write_expenses_to_db()
    delete_empty_rows()
    create_indexes()

